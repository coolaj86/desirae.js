"use strict";

var secretutils = require("secret-utils");

module.exports.sha1sum = function (str) {
  return Promise.resolve(secretutils.hashsum("sha1", str));
};
